# coding: utf-8
# В первой функции мы находим максимальный подмассив, пересекающий центр.
# Во второй функции мы находим максимальный подмассив в левой части, правой и посередине
# после чего сравниваем и возвращаем. O(nlgn)



def find_max_crossing_subarray(a):
  mid = int(len(a)//2)
  left_sum = 0
  sum = 0
  for i in range(mid, 0, -1):
    sum = sum + a[i]
    if sum > left_sum:
      left_sum = sum
      max_left = i

  right_sum = 0
  sum = 0
  for j in range(mid+1, len(a)):
    sum = sum + a[j]
    if sum > right_sum:
      right_sum = sum
      max_right = j
  return max_left, max_right, left_sum + right_sum


def find_max_subarray(a):
  if len(a) == 1:
    return a
  else:
    mid = int(len(a)//2)
    left_low, left_high, left_sum = find_max_crossing_subarray(a[:mid])
    right_low, right_high, right_sum = find_max_crossing_subarray(a[mid:])
    cross_low, cross_high, cross_sum = find_max_crossing_subarray(a)
    if left_sum >= right_sum and left_sum >= cross_sum:
      return left_low, left_high, left_sum
    elif right_sum >= left_sum and right_sum >= cross_sum:
      return right_low, right_high, right_sum
    else:
      return cross_low, cross_high, cross_sum


a = [-1,2,4,6,-5, 6, -7, 8, 9, 10]

print(find_max_subarray(a))
