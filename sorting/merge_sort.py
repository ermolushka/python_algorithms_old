# coding: utf-8 O(nlgn)

def merge_sort(x):
    result = []
    if len(x) < 2:
        return x
    mid = int(len(x)/2)
    left = merge_sort(x[:mid])
    right = merge_sort(x[mid:])
    i = 0
    j = 0
    while i < len(left) and j < len(right):
            if left[i] > right[j]:
                result.append(right[j])
                j += 1
            else:
                result.append(left[i])
                i += 1
    result += left[i:]
    result += right[j:]
    return result

a = [1,2,2,8,2,567,6,3]
print(merge_sort(a))
