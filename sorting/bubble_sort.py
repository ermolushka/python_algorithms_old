# coding: utf-8

# Многократно переставляем попарные соседние элементы


def bubble_sort(a):
  for j in range(len(a)-1,0,-1):
    for i in range(j):
      if a[i] > a[i+1]:
        a[i], a[i+1] = a[i+1], a[i]
  return a


a = [1,2,2,8,2,567,6,3]
print(bubble_sort(a))
