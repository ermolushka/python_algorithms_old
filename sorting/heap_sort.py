# O(nlgn)

def downHeap(a, k, n):
    new_elem = a[k]
    while 2*k+1 < n:
        child = 2*k+1;
        if child+1 < n and a[child] < a[child+1]:
            child += 1
        if new_elem >= a[child]:
            break
        a[k] = a[child]
        k = child
    a[k] = new_elem

def heapsort(a): 
  size = len(a)
  #heapify
  for i in range(size//2-1,-1,-1):
      downHeap(a, i, size)
  #sort
  for i in range(size-1,0,-1):
      temp = a[i]
      a[i] = a[0]
      a[0] = temp
      downHeap(a, 0, i)

a = [7,3,8,2,7,8,4]

print(heapsort(a))
