 # Чтобы сделать это, 
 # она ищет наибольший элемент и помещает его на соответствующую позицию.
 # O(n^2)


def selectionSort(list):
  for j in range(len(list)-1,0,-1):
    posMax = 0
    for i in range(1, j+1):
      if list[i] > list[posMax]:
        posMax = i
    temp = list[j]
    list[j] = list[posMax]
    list[posMax] = temp
  return list


aList = [54,26,93,17,77,31,44,55,20]
print(selectionSort(aList))



