class Node:
  
    def __init__(self,initdata):
        self.data = initdata
        self.next = None

    def getData(self):
        return self.data

    def getNext(self):
        return self.next

    def setNext(self,newnext):
        self.next = newnext


class OrderedList:

    def __init__(self):
        self.head = None

    def isEmpty(self):
        return self.head == None

    def search(self,item):
      current = self.head
      found = False
      stop = False
      while current != None and not found and not stop:
          if current.getData() == item:
              found = True
          else:
              if current.getData() > item:
                  stop = True
              else:
                  current = current.getNext()

      return found

    def add(self,item):
      current = self.head
      previous = None
      stop = False
      while current != None and not stop:
          if current.getData() > item:
              stop = True
          else:
              previous = current
              current = current.getNext()

      temp = Node(item)
      if previous == None:
          temp.setNext(self.head)
          self.head = temp
      else:
          temp.setNext(current)
          previous.setNext(temp)

    def remove(self,item): #O(1)
        current = self.head
        previous = None
        found = False
        while not found:
            if current.getData() == item:
                found = True
            else:
                previous = current
                current = current.getNext()

        if previous == None:
            self.head = current.getNext()
        else:
            previous.setNext(current.getNext())

temp = Node(93)
temp.getData()
mylist = OrderedList()
mylist.add(31)
mylist.add(77)
mylist.add(17)
mylist.add(93)
mylist.add(26)
mylist.add(54)
print(mylist.remove(31))
print(mylist.search(17))
