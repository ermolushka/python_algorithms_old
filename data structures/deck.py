# coding: utf8

# Вероятно, вы также заметили, что в этой реализации 
# добавление и удаление элементов из головы имеет O(1), 
# в то время как те же операции для хвоста - O(n).

class Deque:
    def __init__(self):
        self.items = []

    def isEmpty(self):
        return self.items == []

    def addFront(self, item):
        self.items.append(item)

    def addRear(self, item):
        self.items.insert(0,item)

    def removeFront(self):
        return self.items.pop()

    def removeRear(self):
        return self.items.pop(0)

    def size(self):
        return len(self.items)
