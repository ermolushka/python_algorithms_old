# coding: utf8

# Функция divideBy2 принимает десятичное число в качестве аргумента и последовательно делит его пополам. 
# В строке 7 используется встроенный оператор %, чтобы выделить остаток и в строке 8 поместить его в стек. 
# После того, как процесс деления закончится нулём, в строках 11-13 собирается двоичная последовательность. 
# Строка 11 создаёт пустую строку. Из стека по одному выталкиваются нули и единицы и добавляются к ней справа. В итоге возвращается двоичный результат.

from stacks import Stack
def divideBy2(decNumber):
    remstack = Stack()

    while decNumber > 0:
        rem = decNumber % 2
        remstack.push(rem)
        decNumber = decNumber // 2

    binString = ""
    while not remstack.isEmpty():
        binString = binString + str(remstack.pop())

    return binString

print(divideBy2(42))
