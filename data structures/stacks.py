# coding: utf8
class Stack:
     def __init__(self):
         self.items = []

     def isEmpty(self):
         return self.items == []

     def push(self, item):
         self.items.append(item)

     def pop(self):
         return self.items.pop()

     def peek(self):
         return self.items[len(self.items)-1]

     def size(self):
         return len(self.items)


# s=Stack()

# print(s.isEmpty())
# s.push(4)
# s.push('dog')
# print(s.peek())
# s.push(True)
# print(s.size())
# print(s.isEmpty())
# s.push(8.4)
# print(s.pop())
# print(s.pop())
# print(s.size())

# Эта возможность изменять физическое 
# воплощение абстрактного типа данных при поддержке логических 
# характеристик - пример того, как работает абстракция. Однако, 
# даже если стек будет вести себя аналогично, рассмотрение 
# производительности этих двух реализаций покажет их несомненное 
# различие. Напомним, что операции append и pop обе являются О(1). 
# Это означает, что первая реализация будет выполнять добавление 
# и выталкивание за постоянное время, независимо от количества 
# элементов в стеке. Производительность второго варианта 
# страдает, поскольку и insert(0), и pop(0) для стека, размером n, 
# являются O(n). Очевидно, что даже если в реализации логически эквивалентны,
#  то при тестировании они будут иметь очень разные затраты по времени.

