# coding: utf8
# Предположим, что игрок, держащий картошку, - 
# первый в очереди. После переброса картошки мы просто 
# извлечём его оттуда и тут же поставим обратно, но уже в хвост. 
# Игрок будет ждать, пока все, кто перед ним, побудут первыми, 
# а затем вернётся на это место снова. После num операций 
# извлечений/постановок участник, стоящий впереди, будет удалён 
# окончательно, и цикл начнётся заново. Этот процесс будет 
# продолжаться до тех пор, 
# пока не останется всего одно имя (размер очереди станет равным 1).

from queues import Queue

def hotPotato(namelist, num):
  queue = Queue()
  for name in namelist:
      queue.enqueue(name)

  while queue.size() > 1:
      for i in range(num):
          queue.enqueue(queue.dequeue())

      queue.dequeue()

  return queue.dequeue()

print(hotPotato(["Bill","David","Susan","Jane","Kent","Brad"],7))
